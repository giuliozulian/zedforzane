<?php

/**
 #ddev-generated: Automatically generated WordPress settings file.
 ddev manages this file and may delete or overwrite the file unless this comment is removed.
 */

/** Authentication Unique Keys and Salts. */
define('AUTH_KEY',         'BfPfOXZmoJIxCNDEytVajxfpLoohzGUrfQwDUNihfNjqEnsfYBNGLJTjpSBVEIdt');
define('SECURE_AUTH_KEY',  'nUJqOTjxbqSihMspHBbiAiWXqgQbqKLlgzdGlWOuUVVpMwTatVLQfUBSaaUjHFou');
define('LOGGED_IN_KEY',    'umNxgjpBtUpruNMRPNedWuulruBgLERYIhUFjEHRdKxMVufbuYqwSoczmieQzlaB');
define('NONCE_KEY',        'EjBVGnlOOLcmhrbWEkLOnivqCmfOGSRupunnxACKuQwEmWvyqtOgfsKOxsuyjaCd');
define('AUTH_SALT',        'pEWPmFdCYPkvvcBTTPhTWcykbiYRHaJlegTiFbhRYiEoLuYrUxGRfImwTTwvQUay');
define('SECURE_AUTH_SALT', 'ncwBqpChinIKnMqmvpemfWoYRVoxTbLmesozhbbaOQRQYFqrPplkIpnVYghFKPlG');
define('LOGGED_IN_SALT',   'yuoARhUHwqRkPlWcDgvspvGfSjCsoWZnfeGjRYfvPdYkNlUcWYdhXvAXkbNNqwXm');
define('NONCE_SALT',       'pJZvvGsffroXkSaSqyGfqPEkfUYXmCuhnVUloFgyfBjFcXRnurnBELaZGMwviKGP');

/** Absolute path to the WordPress directory. */
define('ABSPATH', dirname(__FILE__) . '/');

// Include for settings managed by ddev.
$ddev_settings = dirname(__FILE__) . '/wp-config-ddev.php';
if (is_readable($ddev_settings) && !defined('DB_USER') && getenv('IS_DDEV_PROJECT') == 'true') {
  require_once($ddev_settings);
}

/** Include wp-settings.php */
if (file_exists(ABSPATH . '/wp-settings.php')) {
  require_once ABSPATH . '/wp-settings.php';
}
